package com.example.kihara.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "messages")
public class Message {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	@NotBlank(message = "タイトルが未入力です")
	@Size(max = 20, message = "タイトルは２０文字以下で入力してください")
	private String title;

	@Column
	@NotBlank(message = "本文が未入力です")
	@Size(max = 1000, message = "本文は1000文字以下で入力してください")
	private String text;

	@Column
	@NotBlank(message = "カテゴリーを入力してください")
	@Size(max = 10, message = "カテゴリーは10文字以下で入力してください")
	private String category;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "created_date",insertable=false, updatable=false)
	private Date createdDate;

	@Column(name = "updated_date",insertable=false, updatable=true)
	private Date updatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
