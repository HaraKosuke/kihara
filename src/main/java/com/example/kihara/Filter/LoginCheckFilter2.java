package com.example.kihara.Filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LoginCheckFilter2 implements Filter {
	//ログ出力のためのクラス
    private static Log log = LogFactory.getLog( LoginCheckFilter2.class);

    /**
     * 処理(本プログラムではコントローラクラスのメソッド)の前後にログ出力を行うフィルタ定義
     * @param request サーブレットリクエスト
     * @param response サーブレットレスポンス
     * @param chain フィルタチェイン
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response
          , FilterChain chain) throws IOException, ServletException {
        log.debug("LoginCheckFilter2 started.");
        //一連の処理(本プログラムではコントローラクラスのメソッド)を実行
        chain.doFilter(request, response);
        log.debug("LoginCheckFilter2 ended.");
    }

}
