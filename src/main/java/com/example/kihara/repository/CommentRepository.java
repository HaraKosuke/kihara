package com.example.kihara.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.kihara.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
	void deleteById(Integer id);
}
