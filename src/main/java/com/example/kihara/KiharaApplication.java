package com.example.kihara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KiharaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KiharaApplication.class, args);
	}

}
