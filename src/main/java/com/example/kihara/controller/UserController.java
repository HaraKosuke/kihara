package com.example.kihara.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.kihara.entity.Branch;
import com.example.kihara.entity.Department;
import com.example.kihara.entity.User;
import com.example.kihara.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	//Branch、Department情報表示
	@GetMapping("/signUp")
	public ModelAndView sinupView() {
		ModelAndView mav = new ModelAndView();
		//branch情報、departmentを取得
		List<Branch> branchData = userService.findAllBranch();
		List<Department> departmentData = userService.findAllDepartment();

		//画面遷移先を指定
		mav.setViewName("/signUp");

		mav.addObject("branches", branchData);
		mav.addObject("departments", departmentData);
		return mav;
	}

	@PostMapping("/signUpUser")

	public ModelAndView singUp(@ModelAttribute("formModel") User user) {
		ModelAndView mav = new ModelAndView();
		String formUserAccount = user.getAccount();
		User dbUser = userService.findByAccount(formUserAccount);
		if(dbUser != null) {
			mav.addObject("errorMessage", "そのアカウント名はすでに使用されています");
            mav.setViewName("/signUp");
            return mav;
		}


		userService.saveUser(user);

		//画面遷移先を指定
		mav.setViewName("/home");
		return mav;
	}
}
