package com.example.kihara.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.kihara.entity.Message;
import com.example.kihara.entity.User;
import com.example.kihara.repository.MessageRepository;
import com.example.kihara.service.MessageService;

@Controller
public class MessageController {
	@Autowired
	HttpSession session;
	@Autowired
	MessageService messageService;
	@Autowired
	MessageRepository messageRepository;

	@PostMapping("/doPost")
	public ModelAndView commentform(@Validated @ModelAttribute("formModel") Message message, BindingResult result,
			RedirectAttributes attributes) {

		if (result.hasErrors()) {
			List<String> errorList = new ArrayList<String>();
			for (ObjectError error : result.getAllErrors()) {
				errorList.add(error.getDefaultMessage());
			}
			attributes.addFlashAttribute("validationError", errorList);
			return new ModelAndView("redirect:/new");
		}

		//sessionからUser型のobjectを取得
		User user = (User) session.getAttribute("loginUser");
		//コメントしたユーザーidをコメントのentityに格納
		message.setUserId(user.getId());

		//messageServiceに登録に必要な情報を含んだcomment型を引数として渡す
		messageService.saveMessage(message);

		// 画面遷移先を指定
		return new ModelAndView("redirect:/");
	}

	//newpost表示
	@GetMapping("/new")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/newPost");
		return mav;
	}

	@PostMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable Integer id) {
		messageService.deleteMessage(id);

		return  new ModelAndView("redirect:/");
	}
}
