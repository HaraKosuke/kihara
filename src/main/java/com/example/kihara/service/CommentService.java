package com.example.kihara.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.kihara.entity.Comment;
import com.example.kihara.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	// コメント投稿&編集
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	//コメント取得
	public List<Comment> findAllComment() {
		//一応降順で取得
		return commentRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
	}

	//コメント削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
