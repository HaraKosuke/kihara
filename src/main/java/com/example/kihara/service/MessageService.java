package com.example.kihara.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.kihara.entity.Message;
import com.example.kihara.repository.MessageRepository;

@Service
public class MessageService {
	@Autowired
	MessageRepository messageRepository;

	// レコード全件降順で取得
	public List<Message> findAllMessage() {
		return messageRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
	}

	//絞り込みレコード取得
	public List<Message> findBetweenReport(String start1, String end1) {
		SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (start1 != null) {
			start1 = start1 + " " + "00:00:00";
		} else {
			start1 = "2021-07-01" + " " + "00:00:00";
		}
		if (end1 != null) {
			end1 = end1 + " " + "23:59:59";
		} else {
			Date d = new Date();

			String defaultDate = d1.format(d);
			end1 = defaultDate;
		}
		Date since = null;
		try {
			since = d1.parse(start1);
		} catch (ParseException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		Date until = null;
		try {
			until = d1.parse(end1);
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return messageRepository.findBycreatedDateBetween(since, until);
	}
	// コメント投稿&編集
		public void saveMessage(Message message) {
			message.setUpdatedDate(new Date());
			messageRepository.save(message);
		}

		//投稿削除
		public void deleteMessage(Integer id) {
			messageRepository.deleteById(id);
		}




}
