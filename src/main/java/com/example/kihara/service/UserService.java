package com.example.kihara.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kihara.entity.Branch;
import com.example.kihara.entity.Department;
import com.example.kihara.entity.User;
import com.example.kihara.repository.BranchRepository;
import com.example.kihara.repository.DepartmentRepository;
import com.example.kihara.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	BranchRepository branchRepository;

	//Branch全件取得
	public List<Branch> findAllBranch() {
		return branchRepository.findAll();
	}

	//Department全件取得
	public List<Department> findAllDepartment() {
		return departmentRepository.findAll();
	}

	//入力されたアカウントに紐づくユーザ情報取得
	public User findByAccountAndPassword(String account,String password) {
		return userRepository.findByAccountAndPassword(account,password);
	}

	//ユーザー追加
	public void saveUser(User user) {
		userRepository.save(user);
	}
	//ユーザー取得
	public List <User> findAllUser() {
		return userRepository.findAll();
	}
	public User findByAccount(String account){
		return userRepository.findByAccount(account);
	}

}
