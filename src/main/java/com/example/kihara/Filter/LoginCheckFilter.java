package com.example.kihara.Filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.kihara.entity.User;

public class LoginCheckFilter implements Filter {
	//ログ出力のためのクラス
	private static Log log = LogFactory.getLog(LoginCheckFilter.class);

	/**
	 * 処理(本プログラムではコントローラクラスのメソッド)の前後にログ出力を行うフィルタ定義
	 * @param request サーブレットリクエスト
	 * @param response サーブレットレスポンス
	 * @param chain フィルタチェイン
	 * @throws IOException
	 * @throws ServletException
	 */


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

			log.debug("LoginCheckFilter1 started.");
			//一連の処理(本プログラムではコントローラクラスのメソッド)を実行
			// セッションが存在しない場合NULLを返す
			User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");


			if (user!= null) {
				// ログインユーザーがNULLでなければ、通常どおりの遷移
				chain.doFilter(request, response);
			} else {
				// セッションがNullならば、ログイン画面へ飛ばす
				log.debug("不正なアクセス");
				request.setAttribute("errorMessage", "ログインしてください");
				HttpServletResponse httpRes = (HttpServletResponse) response;
	            httpRes.sendRedirect("/login");
			}
		log.debug("LoginCheckFilter1 ended.");
	}

	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void destroy() {
	}

}
