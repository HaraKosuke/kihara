package com.example.kihara.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.kihara.entity.Comment;
import com.example.kihara.entity.User;
import com.example.kihara.service.CommentService;

@Controller
public class CommentController {
	@Autowired
	CommentService commentService;
	@Autowired
	HttpSession session;

	//コメント投稿機能
	@PostMapping("/doComment/{id}")
	public ModelAndView commentform(@Validated @PathVariable Integer id, @ModelAttribute("formModel") Comment comment,RedirectAttributes attributes) {
		ModelAndView mav = new ModelAndView();

		//sessionからUser型のobjectを取得
		User loginUser = (User)session.getAttribute("loginUser");
		//コメントしたユーザーidをコメントのentityに格納
		comment.setUserId(loginUser.getId());
		//メッセージIDが勝手にコメントのIDに入るから消す
		comment.setId(0);
		//コメントする投稿のIDを格納
		comment.setMessageId(id);
		//commentServiceに登録に必要な情報を含んだcomment型を引数として渡す

		commentService.saveComment(comment);
		// 画面遷移先を指定
		return new ModelAndView("redirect:/");
	}
}
