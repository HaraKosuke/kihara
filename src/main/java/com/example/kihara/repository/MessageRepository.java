package com.example.kihara.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.kihara.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer>  {

	List<Message> findBycreatedDateBetween(Date since, Date until);
	void deleteById(Integer id);

}
