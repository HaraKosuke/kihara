package com.example.kihara.controller;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.example.kihara.entity.Comment;
import com.example.kihara.entity.Message;
import com.example.kihara.entity.User;
import com.example.kihara.service.CommentService;
import com.example.kihara.service.MessageService;
import com.example.kihara.service.UserService;

@Controller
//@RequestMapping("/login")
@SessionAttributes("loginuser")
public class ForumController {
	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;
	@Autowired
	HttpSession session;
	@Autowired
	UserService userService;

	//home画面表示機能
	@GetMapping
	public ModelAndView home(@RequestParam(name = "start", required = false) String start,
			@RequestParam(name = "end", required = false) String end) {
		ModelAndView mav = new ModelAndView();
		User loginUser =(User)session.getAttribute("loginUser");

		List<Message> messageData = null;
		if (start != null || end != null) {
			//入力された日時の値を取得
			messageData = messageService.findBetweenReport(start, end);
		} else {
			// 投稿を全件取得
			messageData = messageService.findAllMessage();
		}

		List<Comment> commentData = commentService.findAllComment();
		List<User> userData = userService.findAllUser();

		// 画面遷移先を指定
		mav.setViewName("/home");
		// 投稿データオブジェクトを保管
		mav.addObject("loginUser",loginUser);
		mav.addObject("users", userData);
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		return mav;
	}

	//login画面表示機能

		@GetMapping("/login")
		public ModelAndView login() {
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/login");
			return mav;
		}


	//ログイン処理
	@PostMapping("/loginUser")
	public ModelAndView login(@ModelAttribute("formModel") User user) {

		ModelAndView mav = new ModelAndView();

		String formUserAccount = user.getAccount();
		String formUserPassword = user.getPassword();
		User dbUser = userService.findByAccountAndPassword(formUserAccount,formUserPassword);

		if(dbUser == null) {
			mav.addObject("errorMessage", "ログインに失敗しました");
            mav.setViewName("/login");
            return mav;
		}

        session.setAttribute("loginUser", dbUser);

		//ホーム画面へ遷移
		return new ModelAndView("redirect:/");
	}

	//ログアウト処理
	@GetMapping("/logout")
	public ModelAndView logout() {
		ModelAndView mav = new ModelAndView();

        session.invalidate();

		//ログイン画面へ遷移
		mav.setViewName("/login");
		return mav;
	}


}

