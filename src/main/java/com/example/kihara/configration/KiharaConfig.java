package com.example.kihara.configration;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.kihara.Filter.LoginCheckFilter;

/**
 * アプリケーションの設定を行うクラス
 * 「@Configuration」をクラスに付与し、この中に「@Bean」を付与したメソッドを記述すると、
 * アプリケーション起動時に、「@Bean」を付与したメソッドのコンポーネントが作成される
 */
@Configuration
public class KiharaConfig {


	/**
     * フィルタ1のオブジェクトをコンポーネントに追加
     * @return フィルタ1を登録したBean
     */
    @Bean
    public FilterRegistrationBean loginCheckFilter(){
        //フィルタ1のオブジェクトを1番目に実行するフィルタとして追加
        FilterRegistrationBean bean = new FilterRegistrationBean(new LoginCheckFilter());
        //コントローラ・静的コンテンツ全てのリクエストに対してフィルタ1を有効化
        bean.addUrlPatterns("/signUp","/","/new");
        //フィルタ1の実行順序を1に設定
        bean.setOrder(1);
        return bean;
    }
}
